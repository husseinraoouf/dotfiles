{

  nur = (pkgs: import (fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
    nurpkgs = pkgs;
    pkgs = pkgs;
    repoOverrides = {
      husseinraoouf = import /etc/nixos/private-repo {
        pkgs = pkgs;
      };
    };
  });

  nur-no-pkgs = import (fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
    repoOverrides = {
      husseinraoouf = import /etc/nixos/private-repo {};
    };
  };

}