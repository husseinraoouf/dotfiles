self: super:
with super.lib;
let
  overlays = builtins.attrValues (import ./..);
in
foldl' (flip extends) (_: super) overlays self
