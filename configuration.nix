# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, system, options, ... }:

let
  overlays = builtins.attrValues (import ./overlays);

  pkgs = import <nixpkgs> {
    overlays = overlays;
    config.allowUnfree = true;
  };

  nur-no-pkgs = (import ./nix/nur.nix).nur-no-pkgs;

in
{
  imports =
    [ # Include the results of the hardware scan.
      ./config/hardware-configuration.nix
      ./desktopenviroments/pantheon
      ./users/hussein
    ] ++ builtins.attrValues nur-no-pkgs.repos.husseinraoouf.modules;
    

  _module.args.pkgs = pkgs;
  nixpkgs.config.allowUnfree = true;

  nix.nixPath =
    # Prepend default nixPath values.
    options.nix.nixPath.default ++ 
    # Append our nixpkgs-overlays.
    [ "nixpkgs-overlays=/etc/nixos/overlays/overlays-compat" ]
  ;

  boot.kernelPackages = pkgs.linuxPackages_latest;

  documentation.nixos.enable = false;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware.cpu.intel.updateMicrocode = true;

  networking.hostName = "nixos"; # Define your hostname.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp2s0.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  networking.dhcpcd.wait = "background";
  systemd.services.NetworkManager-wait-online.enable = false;

  systemd.services.systemd-udev-settle.enable = false;
  
  # Set your time zone.
  time.timeZone = "Africa/Cairo";


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
      firefox

      tdesktop
      discord
      
      postman 

      git

      vscode
      dotnet-sdk_3
      unstable.omnisharp-roslyn

      pantheon.elementary-files
      pantheon.elementary-terminal
      pantheon.elementary-screenshot-tool
      pantheon.elementary-code

      nodejs

      nodePackages.node2nix
      

      nur.repos.husseinraoouf.azuredatastudio
      nur.repos.husseinraoouf.mongodb-compass

      openssl
      glibc
      musl

      direnv

      sqlitebrowser
      dbeaver
      tree
    ];
  
  programs.evince.enable = true;
  programs.file-roller.enable = true;

  virtualisation.docker.enable = true;

  services.mongodb = {
    enable = true;
    package = pkgs.mongodb-4_0;
    user = "hussein";
  };
  
  services.postgresql = {
    enable = true;
    authentication = pkgs.lib.mkForce ''
      # Generated file; do not edit!
      local all all              trust
      host  all all 127.0.0.1/32 trust
      host  all all ::1/128      trust
    '';
  };

  systemd.services.mongodb.after = [ "multi-user.target" ];
  systemd.services.docker.after = [ "multi-user.target" ];


  services.sql-server = {
    enable = true;
    saPassword = "123456As@";
  };

  services.lorri.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # disable the firewall altogether.
  networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.webInterface = false;

  # Enable sound.
  sound.enable = true;

  programs.adb.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us,ara";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
  services.xserver.libinput.clickMethod = "buttonareas";
  services.xserver.libinput.scrollMethod = "twofinger";
  services.xserver.libinput.naturalScrolling = true;
  services.xserver.libinput.disableWhileTyping = true;
  services.xserver.libinput.tapping = true;

  
  hardware.bluetooth.powerOnBoot = false;
  
  programs.fish.enable = true;
  programs.bash.enableCompletion = true;
  users.defaultUserShell = "/run/current-system/sw/bin/fish";

  programs.thefuck.enable = true;

  security.rtkit.enable = true;
  services.tlp.enable = true;

  security.sudo.wheelNeedsPassword = false;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

}


